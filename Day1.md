# The description of the module
  * Project
  * Teamwork

# Projects
## Project 1: Online Library Management
Develop an online library management application where users can rent books for a specific time, like them and can also review books. This will have two interfaces:

    - User interface
    - Admin interface

* Users registered for this application can:
    - Browse books from the library
    - Filter them based on category, author, publication, etc.
    - Pay and rent them for a specific duration
    - Like/Review them
* Admin of this application can
    - List and manage books
    - Track rented books and their availability
    - Send notifications via email to users once lease expires

## Project 2: Pharmacy Management System
The aim of this project is to develop an application for the effective management of a pharmaceutical store. It helps the pharmacist to maintain the records of the medicines/drugs and supplies sent in by the supplier. The admin who are handling the organization will be responsible to manage the record of the employee. Each employee will be given with a separate username and password. The users can communicate each other by using a built-in messaging system. Pharmacy management system deals with the maintenance of drugs and consumables in the pharmacy unit. It application can generate invoices, bills, receipts etc.

## Project 3: Child Monitoring System - ASQ (Age and Stages Questionnaires)
The main purpose of this project is to develop an application for monitoring the child development. It records the data of the child responses, from which the recommends for parents can be provided. The questionnaires are following guidelines from [the BESTKC-Best Kids Care](http://www.bestkc.com/wp-content/uploads/2017/05/3-year-ASQ-ENGLISH.pdf) for English version or [the Decision 2254/QĐ-BYT of the ministry of health](https://thuvienphapluat.vn/van-ban/The-thao-Y-te/Quyet-dinh-2254-QD-BYT-2021-Bo-Cong-cu-Phat-hien-som-roi-loan-pho-tu-ky-o-tre-em-473416.aspx) for Vietnamese version. The application allows to visualize the collected data and development progress of the child.

## Project 4: Project proposal
* Write proposal document (word or PDF)
    - Develop an interesting project idea.
    - Pick a short and catchy title.
    - Introduce your project idea.
    - (If any) Discuss your vision: explain what makes your idea different.
    - (If any) Discuss your proposed technical approach: explain what system architecture, technologies, and tools you may use.
    - (If any) Highlight the single most serious challenge or risk you foresee with developing your project on time. How will you minimize or mitigate the risk? Don’t state generic risks that would be equally applicable to any project.

# Architecture
### Three-tier architecture
- From IBM: [Three-tier architecture](https://www.ibm.com/cloud/learn/three-tier-architecture)
>Three-tier architecture is a well-established software application architecture that organizes applications into three logical and physical computing tiers: the presentation tier, or user interface; the application tier, where data is processed; and the data tier, where the data associated with the application is stored and managed.

>__Presentation tier__
>The presentation tier is the user interface and communication layer of the application, where the end user interacts with the application. Its main purpose is to display information to and collect information from the user. This top-level tier can run on a web browser, as desktop application, or a graphical user interface (GUI), for example. Web presentation tiers are usually developed using HTML, CSS and JavaScript. Desktop applications can be written in a variety of languages depending on the platform.

>__Application tier__
>The application tier, also known as the logic tier or middle tier, is the heart of the application. In this tier, information collected in the presentation tier is processed - sometimes against other information in the data tier - using business logic, a specific set of business rules. The application tier can also add, delete or modify data in the data tier.

>__Data tier__
>The data tier, sometimes called database tier, data access tier or back-end, is where the information processed by the application is stored and managed. This can be a relational database management system such as PostgreSQL, MySQL, MariaDB, Oracle, DB2, Informix or Microsoft SQL Server, or in a NoSQL Database server such as Cassandra, CouchDB or MongoDB.

- From Wikipedia: [Three-tier architecture](https://en.wikipedia.org/wiki/Multitier_architecture#/media/File:Overview_of_a_three-tier_application_vectorVersion.svg)

### Microservices architecture
- From Wikipedia:
>A microservice architecture – a variant of the service-oriented architecture (SOA) structural style – arranges an application as a collection of loosely-coupled services. In a microservices architecture, services are fine-grained and the protocols are lightweight. The goal is that teams can bring their services to life independent of others. Loose coupling reduces all types of dependencies and the complexities around it, as service developers do not need to care about the users of the service, they do not force their changes onto users of the service. Therefore it allows organizations developing software to grow fast, and big, as well as use off the shelf services easier. Communication requirements are less. But it comes at a cost to maintain the decoupling. Interfaces need to be designed carefully and treated as a public API. Techniques like having multiple interfaces on the same service, or multiple versions of the same service, to not break existing users code.

# Programming languages
You may use any programming language you like. However, your project must follow professional standards and best practices, such as separation of concerns, modularity, and abstraction.

# Teams (8)
* Project Leader (1)
  - Coordination. Planning. Monitoring. Risk management. 
  - __Testing__:
    - Functionality: test cases and scenarios, for each tier.
    - Integration. 
  - __Documentation__.
* Other members

# Evaluation
- __Project__ [functionality and other quality-related features]. 
Based on final oral presentation (40%).
- __Project report__. Based on final document (40%).
- __Individual contribution__. Based on __gitlab activity__, __class attendance__, __cross-evaluation from other members in the group__, and __individual questions__ (20%).

# Organization
1. Version control: Git repository
[git](https://guides.github.com/activities/hello-world/)

- [ ] Each student creates his/her own gitlab account.
- [ ] Team leader sends to @hthieu the gitlab usernames of the members of his/her team.
- [ ] @hthieu creates private repositories for each team.
- [ ] @hthieu will have access to the private repositories of each team.

2. Documentation: Markdown language [Compulsory]
[Markdown](https://guides.github.com/features/mastering-markdown/)

3. Other Reference Documents:
    -  [API-Swagger]( https://lapp.fly.dev/api/docs/)
    -  [Notion]( https://www.notion.so/kimthanh3001/Programming-exercise-d57211965361401d9b55b67aa3f244c2?pvs=4)
    - [Figma](https://www.figma.com/file/j8tUM7RsajRidYXfPevXfX/LAPP?type=design&node-id=0%3A1&t=W9BHI5PeTJMDZ4kZ-1)
    - [Presentation](https://www.canva.com/design/DAFjIwdxMJM/_-1U09DW2l-COhrZKG-0XQ/view?utm_content=DAFjIwdxMJM&utm_campaign=designshare&utm_medium=link&utm_source=homepage_design_menu) 
